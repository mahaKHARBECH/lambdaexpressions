package unit2;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import unit1.Person;

public class UnitExerciseSolutionJava8Lambda {

	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person("Chris", "Evans", 38),
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Pratt", 41)
				);

		// Step 1 : Sort list by last name
		
		Collections.sort(people, (p1, p2)-> p1.getLastName().compareTo(p2.getLastName()));
		
		// Step 2 : Create a method that prints all elements in the list
		
		performConditionally(people, p -> true, p-> System.out.println(p));
		
		
		// Step 3 :  Create a method that prints all people that have first name beginning with C
		
		performConditionally(people, p-> p.getFirstName().startsWith("C"), p-> System.out.println(p) );
		
		performConditionally(people, p-> p.getLastName().startsWith("C"), p-> System.out.println(p.getFirstName()) );
	}

	
	private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
		for (Person p : people) {
			if (predicate.test(p)) {
			//	System.out.println(p);
				consumer.accept(p);
			}
		}
		System.out.println("--------------------------------------------------");
	}
	

	
}
