package unit2;

public class ClosureExample {

	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		// closure means that the lambda expression takes the frozen val of b if it is used in the scope
		doProcess(a, i -> System.out.println(i+b));
		
		/*doProcess(a, new Process() {
			@Override
			public void process(int i) {
				System.out.println(i + b); // b should be final
			}
		});  */
	}
	
	
	public static void doProcess(int i ,Process p) {
		p.process(i);
	}
}

interface Process {
	void process (int i);
}