package unit2;

public class ThisReferenceCleanExample {
	
	public void doProcess(int i ,Process p) {
		p.process(i);
	}

	public void execute() {
		// this points to the instance of the object in which the execute method is being called 
		// ThisReferenceCleanExample
		// this called inside a lamda expression doesn't change it
		// Contrarily to this called in an anonymous inner class which refer to the latter
		doProcess(10, i-> {
			System.out.println("Value of i is "+ i);
			System.out.println(this);
			});
	}
	public static void main(String[] args) {
		
		ThisReferenceCleanExample thisReferenceExample = new ThisReferenceCleanExample();
		
		thisReferenceExample.doProcess(10, i-> {
			System.out.println("Value of i is "+ i);
			});
		
		thisReferenceExample.execute();
			
	}

	@Override
	public String toString() {
		return "ThisReferenceCleanExample []";
	}
	
}
