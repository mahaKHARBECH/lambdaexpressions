package unit1;
import java.util.Arrays;
import java.util.List;

public class UnitExercise {

	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Evans", 38),
				new Person("Chris", "Pratt", 41)
				);

		// Step 1 : Sort list by last name
		
		// Step 2 : Create a method that prints all elements in the list
		
		// Step 3 :  Create a method that prints all people that have first name beginning with C
		
	}

}
