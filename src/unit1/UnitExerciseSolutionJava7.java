package unit1;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UnitExerciseSolutionJava7 {

	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person("Chris", "Evans", 38),
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Pratt", 41)
				);

		// Step 1 : Sort list by last name
		
		Collections.sort(people, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
		});
		
		// Step 2 : Create a method that prints all elements in the list
		
		printAll(people);
		
		
		// Step 3 :  Create a method that prints all people that have first name beginning with C
		
		printFirstNameBeginingWithC(people);
		
		
		printConditionally(people, new Condition() {
			@Override
			public boolean test(Person p) {
				return p.getLastName().startsWith("C");
			}
		});
	}

	
	private static void printConditionally(List<Person> people, Condition condition) {
		for (Person p : people) {
			if (condition.test(p)) {
				System.out.println(p);
			}
		}
		System.out.println("--------------------------------------------------");
	}
	
	private static void printFirstNameBeginingWithC(List<Person> people) {
		for (Person p : people) {
			if (p.getFirstName().startsWith("C")) {
				System.out.println(p);
			}
		}
		System.out.println("-----------------------------------------");
	}

	private static void printAll(List<Person> people) {
		for (Person p : people) {
			System.out.println(p);
		}
		System.out.println("-----------------------------------------");
	}

}

	interface Condition {
		boolean test(Person p);
}
