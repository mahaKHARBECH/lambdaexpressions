package unit1;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class UnitExerciseSolutionJava8Lambda {
//https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html
	public static void main(String[] args) {
		
		List<Person> people = Arrays.asList(
				new Person("Chris", "Evans", 38),
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Pratt", 41)
				);

		// Step 1 : Sort list by last name
		
		Collections.sort(people, (p1, p2)-> p1.getLastName().compareTo(p2.getLastName()));
		
		// Step 2 : Create a method that prints all elements in the list
		
		printConditionally(people, p -> true);
		
		
		// Step 3 :  Create a method that prints all people that have first name beginning with C
		
		printConditionally(people, p-> p.getFirstName().startsWith("C") );
		
		printConditionally(people, p-> p.getLastName().startsWith("C") );
	}

	
	private static void printConditionally(List<Person> people, Condition condition) {
		for (Person p : people) {
			if (condition.test(p)) {
				System.out.println(p);
			}
		}
		System.out.println("--------------------------------------------------");
	}
	

	
}
