package unit3;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import unit1.Person;

public class MethodReferenceExample2 {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Chris", "Evans", 38),
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Pratt", 41)
				);
		
		//performConditionally(people, p -> true, p-> System.out.println(p));
		performConditionally(people, p -> true, System.out::println);  // p -> method(p)
	}

	
	private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
		for (Person p : people) {
			if (predicate.test(p)) {
				consumer.accept(p);
			}
		}
	}
	

}
