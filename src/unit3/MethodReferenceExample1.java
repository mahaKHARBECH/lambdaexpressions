package unit3;

public class MethodReferenceExample1 {

	public static void main(String[] args) {
		Thread t = new Thread(()->printMessage());
		t.start();

		//MethodReferenceExample1::printMessage === ()->printMessage()
		Thread t1 = new Thread(MethodReferenceExample1::printMessage); //()-> method
		t1.start();

	
	}

	
	public static void printMessage() {
		System.out.println("Hello");
		
	}
}
