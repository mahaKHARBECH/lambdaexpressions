package unit3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import unit1.Person;

public class StreamsExample {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Evans", 38),
				new Person("Chris", "Pratt", 41)
				);

		people.stream().
		filter(p->p.getLastName().startsWith("C")).
		forEach(p->System.out.println(p.getFirstName()));
		
		// stream we have the runtime doing the iteration
		long count = people.stream().
		filter(p->p.getLastName().startsWith("C"))
		.count();
		
		long count1 = people.parallelStream().
				filter(p->p.getLastName().startsWith("C"))
				.count();
		
		System.out.println(count);
		System.out.println(count1);
	}

}
