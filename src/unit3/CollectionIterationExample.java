package unit3;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import unit1.Person;

public class CollectionIterationExample {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Leo", "DiCaprio", 43),
				new Person("Henry", "Cavill", 34),
				new Person("Chris", "Evans", 38),
				new Person("Chris", "Pratt", 41)
				);

		// external iterator
		System.out.println("Using for loop");
		for (int i = 0; i < people.size(); i++) {
			System.out.println(people.get(i));
		}
		// external iterator
		System.out.println("Using for in loop");
		for (Person p : people) {
			System.out.println(p);
		}
		
		// internal iterator
		System.out.println("Using lambda for each loop");
	//	people.forEach(p ->System.out.println(p));  // p in people
		people.forEach(System.out::println);
	}

}
